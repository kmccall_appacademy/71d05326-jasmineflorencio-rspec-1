def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  arr << 0
  arr.reduce(:+)
end

def multiply(*num)
  num.reduce(:*)
end

def power(base_num, raise_num)
  base_num**raise_num
end

def factorial(num)
  return 1 if num == 0
  factors = (1..num).to_a
  factors.reduce(:*)
end
