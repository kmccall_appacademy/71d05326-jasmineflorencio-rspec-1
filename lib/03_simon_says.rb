def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(word, times = 2)
  Array.new(times, word).join(" ")
end

def start_of_word(word, num_start)
  word[0...num_start]
end

def first_word(string)
  string.split(" ")[0]
end

def titleize(string)
  lowercase_words =
    ["a", "an", "the", "at", "by", "for", "in", "of", "over", "on",
     "to", "up", "and", "as", "but", "or", "nor"]
  title = Array.new
  
  string.split(" ").each_with_index do |word, idx|
    if idx == 0
      title << word.capitalize
    elsif lowercase_words.include?(word)
      title << word.downcase
    else
      title << word.capitalize
    end
  end

  title.join(" ")
end
