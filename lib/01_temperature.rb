def ftoc(ftemp)
  ctemp = (ftemp - 32) * (5.0 / 9.0)
  return ctemp.to_i if ctemp == ctemp.to_i
  ctemp
end

def ctof(ctemp)
  ftemp = (ctemp * 1.8) + 32
  return ftemp.to_i if ftemp == ftemp.to_i
  ftemp
end
