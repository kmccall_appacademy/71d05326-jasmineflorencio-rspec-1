def translate(sentence)
  translated_array = Array.new
  sentence.split(" ").each do |translate_word|
    if translate_word[0] == translate_word[0].upcase
      translated_array << punctuation_helper(translate_word).capitalize
    else
      translated_array << punctuation_helper(translate_word)
    end
  end
  translated_array.join(" ")
end

def pig_helper(word)
  vowels = "aeiou"
  word.each_char.with_index do |letter, idx|
    if vowels.include?(letter)
      next if word[idx - 1] == "q"
      return word[idx..-1] + word[0...idx] + "ay"
    end
  end
end

def punctuation_helper(word)
  all_punc = word.split("").select { |punc| punc =~ /[.?!,:;]/ }
  pig_helper(word.delete(".?!,:;")) + all_punc.join("")
end
